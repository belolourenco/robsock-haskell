module SimpleExample where

import RobSock.RobSockMonad
import RobSock.RobSockUtils
import RobSock.RobSock(BeaconMeasure(BM))

import Control.Monad
import Control.Monad.State
import Control.Monad.Trans
import Control.Applicative
import Data.Maybe(fromJust)
import System.Environment
import Debug.Trace

debug = True

debugShow :: String -> MM ()
debugShow x = case debug of
                True  -> lift.putStrLn $ x
                False -> return ()

main :: IO ()
main = do [name,pos,host] <- getArgs
          (x,_) <- runStateT (start name (read pos) host) NoMouse
          case x of
            (-1) -> putStrLn "Failed to connect"
            _    -> putStrLn "Connection finished"

start :: String -> Int -> String -> MM Int
start n p h = do get >>= debugShow.show
                 x <- initRobot n p h 
                 case x of
                   (-1) -> return $ -1
                   y    -> waitStart >> return 0

waitStart:: MM ()
waitStart = do get >>= debugShow.show
               readSensors
               getLSpeed >>= \l -> getRSpeed 
                         >>= debugShow.(("Speed:" ++ show l) ++).show
               x <- getStartButton 
               case x of
                 True  -> movingToCheese 0
                 False -> waitStart

movingToCheese :: Int -> MM ()
movingToCheese c = do get >>= debugShow.show  
                      x <- fetchMessages >>= debugShow.show
                      readSensors
                      sensors <- getSensors
                      debugShow.show $ sensors
                      if isInTarget sensors
                        then setVisitingLed True >> (lift.putStrLn $ "Found the beacon")
                                                 >> waitOthers (c+1)
                        else do decideWhatToDo sensors
                                if (c `mod` 8 == 0) then reqObstacleBeacon 
                                                    else reqObstacleGround
                                movingToCheese (c+1)
  where isInTarget s = case ground s of
                         Nothing -> False
                         (Just i)-> if i == 0 then True else False

waitOthers :: Int -> MM ()
waitOthers c = do readSensors
                  sensors <- getSensors
                  debugShow.show $ sensors
                  x <- getReturningLed
                  case x of
                    False -> waitOthers (c+1)
                    True  -> (lift.putStrLn $ "moving home") >> movingToHome (c+1)

movingToHome :: Int -> MM ()
movingToHome c = do get >>= debugShow.show  
                    x <- fetchMessages >>= debugShow.show
                    readSensors
                    sensors <- getSensors
                    debugShow.show $ sensors
                    if isInTarget sensors
                      then finish >> (lift.putStrLn $ "Finish")
                      else do decideWhatToDo sensors
                              if (c `mod` 4 == 0) then reqObstacleHomeBeacon 
                                                  else reqObstacleGround
                              movingToHome (c+1)
  where isInTarget s = case ground s of
                         Nothing -> False
                         (Just i)-> if i == 1 then True else False

decideWhatToDo :: Sensors -> MM ()
decideWhatToDo s = if bumperTrue s
                   then if mb2d (obsLeft s) > mb2d (obsRight s)
                        then rotate (pi/4) RotRight 
                        else rotate (pi/4) RotLeft
                   else if mb2d (obsCenter s) > 3.5
                        then if mb2d (obsLeft s) > mb2d (obsRight s)
                             then rotate (pi/4) RotRight
                             else rotate (pi/4) RotLeft
                        else if mb2d (obsLeft s) > 3.5
                             then rotate (pi/4) RotRight
                             else if mb2d (obsRight s) > 3.5 
                                  then rotate (pi/4) RotLeft
                                  else decideByBeacon $ beacon s
  where mb2d :: Maybe Double -> Double
        mb2d Nothing = 0
        mb2d (Just x)= x

        decideByBeacon :: Maybe BeaconMeasure -> MM ()
        decideByBeacon Nothing             = driveMotors 0.1 0.1
        decideByBeacon (Just (BM False _)) = driveMotors 0.1 0.1
        decideByBeacon (Just (BM _ dir)) | dir > 0 = debugShow ("I Smeel Beacon left " ++ (show dir)) 
                                                     >> rotate (dir*pi/180) RotLeft
                                         | dir < 0 = debugShow ("I Smeel Beacon right " ++ (show dir) )
                                                     >> rotate ((abs dir)*pi/180) RotRight 

bumperTrue :: Sensors -> Bool
bumperTrue s = case bumper s of
                 Nothing -> False
                 (Just b)-> b

reqObstacleOnly :: MM ()
reqObstacleOnly = requestSensors ["IRSensor0", "IRSensor1", "IRSensor2"]

reqObstacleBeacon :: MM ()
reqObstacleBeacon = requestSensors ["IRSensor0", "IRSensor1", "IRSensor2", "Beacon0"]

reqObstacleHomeBeacon :: MM ()
reqObstacleHomeBeacon = getNumberOfBeacons 
                      >>= \x -> requestSensors ["IRSensor0", "IRSensor1", "IRSensor2", "Beacon"++(show x)]

reqObstacleGround = requestSensors ["IRSensor0", "IRSensor1", "IRSensor2", "Ground"]
