{-# LANGUAGE GeneralizedNewtypeDeriving #-}
module RobSock.RobSockMonad where

import qualified RobSock.RobSock as R

import Control.Monad.State
import Control.Monad.Trans
import Control.Applicative

data Mouse = Mouse { mName    :: String   -- ^ Robot Name
                   , mPos     :: Int      -- ^ Position of the robot in the starting grid
                   , mHost    :: String   -- ^ Simulator host
                   , mlPow    :: Double   -- ^ Left wheel power
                   , mrPow    :: Double   -- ^ Right wheel power
                  }
           | NoMouse
           deriving (Show)

updatePower :: Double -> Double -> Mouse -> Mouse
updatePower l r (Mouse n p h _ _) = Mouse n p h l r

type MM = StateT Mouse IO

------------------------------------------------------------------------------
-- * Constants

-- #define CENTER 0
-- #define LEFT   1
-- #define RIGHT  2
-- #define OTHER1 3

-- | Constant value with the center sensor ID
center :: Int
center = R.center 

-- | Constant value with the left sensor ID
left   :: Int
left   = R.left

-- | Constant value with the right sensor ID
right  :: Int
right  = R.right 

-- | Constant value with the other1 sensor ID
other1 :: Int
other1 = R.other1 


------------------------------------------------------------------------------
-- * Initialization

-- | Initializes Robot and Connects to Simulator 
-- Returns -1 in case of error
initRobot :: String -- ^ Robot name
          -> Int    -- ^ Robot position
          -> String -- ^ Host where the simulator is running
          -> MM Int
initRobot n p h = do put (Mouse n p h 0 0)
                     lift $ R.initRobot n p h

-- | Initializes Robot and Connects to Simulator 
-- Returns -1 in case of error
initRobot2 :: String     -- ^ Robot name                          
           -> Int        -- ^ Robot position
           -> [Double]   -- ^ IRSensorAngles
           -> String     -- ^ Host where the simulator is running
           -> MM Int
initRobot2 n p i h = do put (Mouse n p h 0 0)
                        lift $ fromIntegral <$> R.initRobot2 n p i h
                        
-- | Initializes Robot that also works as the beacon and Connects to Simulator 
--   Returns -1 in case of error
--   extern int InitRobotBeacon(char *name, int id, double height, char *host);
initRobotBeacon :: String  -- ^ Robot name
                -> Int     -- ^ Robot position
                -> Double  -- ^ Height of beacon
                -> String  -- ^ Host where the simulator is running
                -> MM Int
initRobotBeacon n p he h = do put (Mouse n p h 0 0) 
                              lift $ fromIntegral <$> R.initRobotBeacon n p he h

------------------------------------------------------------------------------
-- * Sensors

-- | Gets the next Sensor Values sent by Simulator 
-- if no message is present waits 
-- Returns -1 in case of error 
readSensors :: MM Int
readSensors = lift R.readSensors

-- ** The following functions access values that have been read by ReadSensors() 
-- they do not read new values 

-- |  simulation time
getTime :: MM Int
getTime = lift R.getTime

-- | Indicates if a new Obstacle measure from sensor id has arrived. 
-- The value of GetObstacleSensor is invalid when IsObstacleReady returns false
isObstacleReady :: Int     -- ^ id
                -> MM Bool
isObstacleReady id = lift $ R.isObstacleReady id

-- | GetObstacleSensor value is inversely proportional to obstacle distance  
-- id may be one of LEFT, RIGHT, CENTER or OTHER1 
getObstacleSensor :: Int        -- ^ id
                  -> MM Double
getObstacleSensor id = lift $ R.getObstacleSensor id

-- | Indicates if a new beacon measure has arrived. 
-- The value of GetBeaconSensor is invalid when IsBeaconReady returns false
isBeaconReady :: Int      -- ^ id
              -> MM Bool
isBeaconReady id = lift $ R.isBeaconReady id
 
-- | GetNumberOfBeacons returns the number of beacons in the lab
getNumberOfBeacons :: MM Int
getNumberOfBeacons = lift R.getNumberOfBeacons

-- | GetBeaconSensor value is the direction of Beacon 
--  in Robot coordinates (-180.0, 180.0) 
getBeaconSensor :: Int -> MM R.BeaconMeasure
getBeaconSensor id = lift $ R.getBeaconSensor id

-- | Indicates if a new compass measure has arrived. 
-- The value of GetCompassSensor is invalid when IsCompassReady returns false
isCompassReady :: MM Bool
isCompassReady = lift R.isCompassReady

-- | GetCompassSensor value is the direction of Robot in Ground 
-- coordinates (-180.0, 180.0) 
getCompassSensor :: MM Double
getCompassSensor = lift R.getCompassSensor

-- | Indicates if a new ground measure has arrived. 
--  The value of GetGroundSensor is invalid when IsGroundReady returns false
isGroundReady :: MM Bool
isGroundReady = lift R.isGroundReady

-- | If robot is inside a target area returns the id of the area, 
-- otherwise returns -1 
getGroundSensor :: MM Int
getGroundSensor = lift R.getGroundSensor

-- | Indicates if a new bumper measure has arrived. 
-- The value of GetBumperSensor is invalid when IsBumperReady returns false
isBumperReady :: MM Bool
isBumperReady = lift R.isBumperReady

-- | Active when robot collides
getBumperSensor :: MM Bool
getBumperSensor = do x <- lift R.getBumperSensor
                     if x == True
                       then modify (updatePower 0 0) >> return x
                       else return x

-- | Indicates if a new GPS measure has arrived. 
isGPSReady :: MM Bool
isGPSReady = lift R.isGPSReady

-- | Indicates if a new GPS Direction measure has arrived. 
isGPSDirReady :: MM Bool
isGPSDirReady = lift R.isGPSDirReady

-- | Get coordinate X from GPS sensor
-- GPS sensor - can be used for debug, invoke simulator with "-gps" option
getX :: MM Double
getX = lift R.getX 

-- | Get coordinate Y from GPS sensor
-- The value of getY is invalid when isGPSReady returns false
-- GPS sensor - can be used for debug, invoke simulator with "-gps" option
getY :: MM Double
getY = lift R.getY

-- | Get coordinate Direction from GPS sensor
-- The value of getDir is invalid when isGPSDirReady returns false
-- GPS sensor - can be used for debug, invoke simulator with "-gps" option
getDir :: MM Double
getDir = lift R.getDir

-- | Checks if a new message has arrived
newMessageFrom :: Int     -- ^ id
               -> MM Bool
newMessageFrom id = lift $ R.newMessageFrom id

-- | Gets message
getMessageFrom :: Int       -- ^ id
               -> MM String
getMessageFrom id = lift $ R.getMessageFrom id


-- | Request a list of nReqs Sensors
-- Sensors are identified by the following strings: 
-- "Compass", "Ground", "Collision", "IRSensor0", "IRSensor1", etc, "Beacon0", "Beacon1", etc
requestSensors :: [String] -> MM ()
requestSensors = lift.R.requestSensors

------------------------------------------------------------------------------
-- * Buttons

-- | Start   
getStartButton :: MM Bool
getStartButton = lift R.getStartButton


-- | Stop
getStopButton :: MM Bool
getStopButton = lift R.getStopButton

------------------------------------------------------------------------------
-- * Actions

-- | Drive right motor with rPow and left motor with lPow
-- Powers in (-0.15,0.15)
driveMotors :: Double -- ^ lPow
            -> Double -- ^ rPow
            -> MM ()
driveMotors l r = do let l' = if l > 0.15 then 0.15 
                              else (if l < (-0.15) then (-0.15) else l)
                     let r' = if r > 0.15 then 0.15 
                              else (if r < (-0.15) then (-0.15) else r)
                     mouse <- get
                     let lOut = ((mlPow mouse) + l') / 2
                     let rOut = ((mrPow mouse) + r') / 2
                     modify $ updatePower lOut rOut
                     lift $ R.driveMotors l' r'

-- | Signal the end of phase 1 (go to target)
setReturningLed :: Bool  -- ^ val
                -> MM ()
setReturningLed b = lift $ R.setReturningLed b

-- /* Set the state of visiting Led */
setVisitingLed :: Bool -> MM ()
setVisitingLed b = lift $ R.setVisitingLed b

-- | Finish the round
finish :: MM ()
finish = lift R.finish

-- /* Broadcast message */
say :: String -- ^ Message
    -> MM ()
say s = lift $ R.say s

------------------------------------------------------------------------------
-- ** Requests

requestCompassSensor :: MM ()
requestCompassSensor = lift R.requestCompassSensor

requestGroundSensor :: MM ()
requestGroundSensor = lift R.requestGroundSensor  

requestObstacleSensor :: Int    -- ^ id
                      -> MM ()
requestObstacleSensor id = lift $ R.requestObstacleSensor id

requestBeaconSensor :: Int    -- ^ id
                    -> MM ()
requestBeaconSensor id = lift $ R.requestBeaconSensor id

------------------------------------------------------------------------------
-- * Checking Actions

-- | Verify if setReturningLed was executed
getReturningLed :: MM Bool
getReturningLed = lift R.getReturningLed

-- | Verify state of Visiting Led 
getVisitingLed :: MM Bool
getVisitingLed = lift R.getVisitingLed

-- | Verify if Finish() was executed
getFinished :: MM Bool
getFinished = lift R.getFinished

------------------------------------------------------------------------------
-- * Parameters

-- | Return the simulation cycle time
getCycleTime :: MM Int
getCycleTime = lift R.getCycleTime

-- | Return the total simulation time
getFinalTime :: MM Int
getFinalTime = lift R.getFinalTime 

-- | Return the key time
getKeyTime :: MM Int
getKeyTime = lift R.getKeyTime

------------------------------------------------------------------------------
-- ** Functions returning noise levels

-- | Returns maximum additive noise of infra-red sensors
getNoiseObstacleSensor :: MM Double
getNoiseObstacleSensor = lift R.getNoiseObstacleSensor

-- | Returns maximum additive noise of beacon angular direction
getNoiseBeaconSensor :: MM Double
getNoiseBeaconSensor = lift R.getNoiseBeaconSensor

-- | Returns maximum additive noise of compass
getNoiseCompassSensor :: MM Double
getNoiseCompassSensor = lift R.getNoiseCompassSensor

-- | Returns maximum multipicative noise of motors
getNoiseMotors :: MM Double
getNoiseMotors = lift R.getNoiseMotors

-- unsigned int  GetNumberRequestsPerCycle(void);
getNumberRequestsPerCycle :: MM Int
getNumberRequestsPerCycle = lift R.getNumberRequestsPerCycle

-- double GetBeaconAperture();
getBeaconAperture :: MM Int
getBeaconAperture = lift R.getBeaconAperture
 
-- unsigned int GetBeaconLatency();
getBeaconLatency :: MM Int
getBeaconLatency = lift R.getBeaconLatency

-- unsigned int GetIRLatency();
getIRLatency :: MM Int
getIRLatency = lift R.getIRLatency

-- unsigned int GetGroundLatency();
getGroundLatency :: MM Int
getGroundLatency = lift R.getGroundLatency

-- unsigned int GetBumperLatency();
getBumperLatency :: MM Int
getBumperLatency = lift R.getBumperLatency

-- bool GetBeaconRequestable();
getBeaconRequestable :: MM Int
getBeaconRequestable = lift R.getBeaconRequestable

-- bool GetIRRequestable();
getIRRequestable :: MM Int
getIRRequestable = lift R.getIRRequestable

-- bool GetGroundRequestable();
getGroundRequestable :: MM Int
getGroundRequestable = lift R.getGroundRequestable

-- bool GetBumperRequestable();
getBumperRequestable :: MM Int
getBumperRequestable = lift R.getBumperRequestable

------------------------------------------------------------------------------
-- * Functions over state

-- | Returns the @Mouse@ being used
getMouse :: MM Mouse
getMouse = get >>= return

-- | Return the Name of the robot
getName :: MM String
getName = get >>= return.mName

-- | Returns the position of the robot
getPos :: MM Int
getPos = get >>= return.mPos

-- | Return the host being used
getHost :: MM String
getHost = get >>= return.mHost

-- | Returns the mouse left wheel power
getLSpeed :: MM Double
getLSpeed = get >>= return.mlPow

-- | Returns the mouse right wheel power
getRSpeed :: MM Double
getRSpeed = get >>= return.mrPow

