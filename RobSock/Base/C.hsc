{-# LANGUAGE ForeignFunctionInterface #-}

module RobSock.Base.C where

import Foreign
import Foreign.C.Types
import Foreign.C.String
import Control.Applicative

#include"RobSock.h"
#include"aux.h"

------------------------------------------------------------------------------
-- * Constants

-- #define CENTER 0
-- #define LEFT   1
-- #define RIGHT  2
-- #define OTHER1 3

#{enum CInt, CInt
     , c_center  = CENTER 
     , c_left    = LEFT   
     , c_right   = RIGHT  
     , c_other1  = OTHER1 
}

------------------------------------------------------------------------------
-- * Initialization

-- | Initializes Robot and Connects to Simulator 
-- Returns -1 in case of error
foreign import ccall safe "InitRobot"
  c_initRobot :: CString -- ^ Robot name
              -> CInt    -- ^ Robot position
              -> CString -- ^ Host where the simulator is running
              -> IO CInt

-- | Initializes Robot and Connects to Simulator 
-- Returns -1 in case of error
foreign import ccall safe "InitRobot2"
  c_initRobot2 :: CString     -- ^ Robot name                          
               -> CInt        -- ^ Robot position
               -> Ptr CDouble -- ^ IRSensorAngles
               -> CString     -- ^ Host where the simulator is running
               -> IO CInt

 
-- | Initializes Robot that also works as the beacon and Connects to Simulator 
--   Returns -1 in case of error
--   extern int InitRobotBeacon(char *name, int id, double height, char *host);
foreign import ccall safe "InitRobotBeacon"
  c_initRobotBeacon :: CString  -- ^ Robot name
                    -> CInt     -- ^ Robot position
                    -> CDouble  -- ^ Height of beacon
                    -> CString  -- ^ Host where the simulator is running
                    -> IO CInt

------------------------------------------------------------------------------
-- * Sensors

-- | Gets the next Sensor Values sent by Simulator 
-- if no message is present waits 
-- Returns -1 in case of error 
foreign import ccall safe "ReadSensors"
  c_readSensors :: IO CInt

-- ** The following functions access values that have been read by ReadSensors() 
-- they do not read new values 

-- |  simulation time
foreign import ccall "GetTime"
  c_getTime :: IO CUInt

-- | Indicates if a new Obstacle measure from sensor id has arrived. 
-- The value of GetObstacleSensor is invalid when IsObstacleReady returns false
foreign import ccall safe "IsObstacleReady"
  c_isObstacleReady :: CInt       -- ^ id
                    -> IO CUInt   -- ATTENTION! the return value is bool in c++

-- | GetObstacleSensor value is inversely proportional to obstacle distance  
-- id may be one of LEFT, RIGHT, CENTER or OTHER1 
foreign import ccall safe "GetObstacleSensor"
  c_getObstacleSensor :: CInt       -- ^ id
                      -> IO CDouble

-- | Indicates if a new beacon measure has arrived. 
-- The value of GetBeaconSensor is invalid when IsBeaconReady returns false
-- 
-- extern bool            IsBeaconReady(int id);
foreign import ccall safe "IsBeaconReady"
  c_isBeaconReady :: CInt
                  -> IO CUInt

 
-- | GetNumberOfBeacons returns the number of beacons in the lab
foreign import ccall safe "GetNumberOfBeacons"
  c_getNumberOfBeacons :: IO CInt

-- | struct beaconMeasure {
--   bool   beaconVisible;  /* true if robot can see beacon */
--   double beaconDir;      /* direction of beacon */
--                          /*   only valid if beaconVisible is true */
-- };
data CBeaconMeasure = CBeaconMeasure {
    c_beaconVisible :: CUChar
 ,  c_beacondir     :: CDouble
}
  deriving (Show, Eq)

#let alignment t = "%lu", (unsigned long)offsetof(struct {char x__; t (y__); }, y__)

instance Storable CBeaconMeasure where
  sizeOf    _     = #{size struct beaconMeasure}
  alignment _     = #{alignment struct beaconMeasure}    -- alignment (undefined :: CUInt)
  poke ptr (CBeaconMeasure c d) = do
      #{poke struct beaconMeasure, beaconVisible} ptr c
      #{poke struct beaconMeasure, beaconDir}     ptr d
  -- error "poke from Storable CBeaconMeasure not implemented"
  -- peekByteOff :: Ptr b -> Int -> IO a
  --peekByteOff p x = 
  -- peek :: Ptr a -> IO a
  peek p          = return CBeaconMeasure 
                    <*> (#{peek struct beaconMeasure, beaconVisible} p)
                    <*> (#{peek struct beaconMeasure, beaconDir} p)

-- | Value is the direction of the beacon in Robot coordinates (-180,180) 
foreign import ccall safe "GetBeaconSensorWrapper"
   c_getBeaconSensor :: CInt                    -- ^ id
                     -> IO (Ptr CBeaconMeasure)

-- | Release memory allocated for the BeaconSensor
foreign import ccall safe "FreeBeaconSensorWrapper"
   c_freeBeaconSensor :: Ptr CBeaconMeasure
                     -> IO ()

-- | Indicates if a new compass measure has arrived. 
-- The value of GetCompassSensor is invalid when IsCompassReady returns false
foreign import ccall safe "IsCompassReady"
  c_isCompassReady :: IO CUInt

-- | GetCompassSensor value is the direction of Robot in Ground 
-- coordinates (-180.0, 180.0) 
foreign import ccall safe "GetCompassSensor"
  c_getCompassSensor :: IO CDouble

-- | Indicates if a new ground measure has arrived. 
--  The value of GetGroundSensor is invalid when IsGroundReady returns false
foreign import ccall safe "IsGroundReady"
  c_isGroundReady :: IO CUInt

-- | If robot is inside a target area returns the id of the area, 
-- otherwise returns -1 
foreign import ccall safe "GetGroundSensor"
  c_getGroundSensor :: IO CInt


-- | Indicates if a new bumper measure has arrived. 
-- The value of GetBumperSensor is invalid when IsBumperReady returns false
foreign import ccall safe "IsBumperReady"
  c_isBumperReady :: IO CUInt

-- | Active when robot collides
foreign import ccall safe "GetBumperSensor"
  c_getBumperSensor :: IO CUInt

-- | Indicates if a new GPS measure has arrived. 
foreign import ccall safe "IsGPSReady"
  c_isGPSReady :: IO CUInt

-- | Indicates if a new GPS Direction measure has arrived. 
foreign import ccall safe "IsGPSDirReady"
  c_isGPSDirReady :: IO CUInt

-- | Get coordinate X from GPS sensor
-- The value of getX is invalid when isGPSReady returns false
-- GPS sensor - can be used for debug, invoke simulator with "-gps" option
foreign import ccall safe "GetX"
  c_getX :: IO CDouble

-- | Get coordinate Y from GPS sensor
-- The value of getY is invalid when isGPSReady returns false
-- GPS sensor - can be used for debug, invoke simulator with "-gps" option
foreign import ccall safe "GetY"
  c_getY :: IO CDouble

-- | Get coordinate Direction from GPS sensor
-- The value of getDir is invalid when isGPSDirReady returns false
-- GPS sensor - can be used for debug, invoke simulator with "-gps" option
foreign import ccall safe "GetDir"
  c_getDir :: IO CDouble

-- | Checks if a new message has arrived
foreign import ccall safe "NewMessageFrom"
  c_newMessageFrom :: CInt     -- ^ id 
                   -> IO CUInt 

-- | Gets message
foreign import ccall safe "GetMessageFrom"
  c_getMessageFrom :: CInt    -- ^ id
                   -> IO CString

-- Request a list of nReqs Sensors
-- Sensors are identified by the following strings: 
-- "Compass", "Ground", "Collision", "IRSensor0", "IRSensor1", etc, "Beacon0", "Beacon1", etc
--
-- void RequestSensors(int nReqs, ...);

-- | Request one sensor
foreign import ccall safe "RequestSensors"
   c_requestSensors1 :: CInt -> CString -> IO ()

-- | Request two sensors
foreign import ccall safe "RequestSensors"
   c_requestSensors2 :: CInt -> CString -> CString -> IO ()

-- | Request three sensors
foreign import ccall safe "RequestSensors"
   c_requestSensors3 :: CInt -> CString -> CString -> CString -> IO ()

-- | Request four sensors
foreign import ccall safe "RequestSensors"
   c_requestSensors4 :: CInt -> CString -> CString -> CString -> CString -> IO ()

-- | Request five sensors
foreign import ccall safe "RequestSensors"
   c_requestSensors5 :: CInt -> CString -> CString -> CString -> CString 
                             -> CString -> IO ()

-- | Request six sensors
foreign import ccall safe "RequestSensors"
   c_requestSensors6 :: CInt -> CString -> CString -> CString -> CString
                             -> CString -> CString -> IO ()

-- | Request seven sensors
foreign import ccall safe "RequestSensors"
   c_requestSensors7 :: CInt -> CString -> CString -> CString -> CString
                             -> CString -> CString -> CString -> IO ()

-- | Request eight sensors
foreign import ccall safe "RequestSensors"
   c_requestSensors8 :: CInt -> CString -> CString -> CString -> CString
                             -> CString -> CString -> CString -> CString -> IO ()

-- | Request nine sensors
foreign import ccall safe "RequestSensors"
   c_requestSensors9 :: CInt -> CString -> CString -> CString -> CString
                             -> CString -> CString -> CString -> CString
                             -> CString -> IO ()

-- | Request ten sensors
foreign import ccall safe "RequestSensors"
   c_requestSensors10 :: CInt -> CString -> CString -> CString -> CString
                              -> CString -> CString -> CString -> CString
                              -> CString -> CString -> IO ()

-- | Request eleven sensors
foreign import ccall safe "RequestSensors"
   c_requestSensors11 :: CInt -> CString -> CString -> CString -> CString
                              -> CString -> CString -> CString -> CString
                              -> CString -> CString -> CString -> IO ()

-- | Request twelve sensors
foreign import ccall safe "RequestSensors"
   c_requestSensors12 :: CInt -> CString -> CString -> CString -> CString
                              -> CString -> CString -> CString -> CString
                              -> CString -> CString -> CString -> CString
                              -> IO ()

-- | Request thirteen sensors
foreign import ccall safe "RequestSensors"
   c_requestSensors13 :: CInt -> CString -> CString -> CString -> CString
                              -> CString -> CString -> CString -> CString
                              -> CString -> CString -> CString -> CString
                              -> CString -> IO ()

------------------------------------------------------------------------------
-- * Buttons

-- | Start   
foreign import ccall safe "GetStartButton"
  c_getStartButton :: IO CUInt


-- | Stop
foreign import ccall safe "GetStopButton"
  c_getStopButton :: IO CUInt

------------------------------------------------------------------------------
-- * Actions

-- | Drive right motor with rPow and left motor with lPow
-- Powers in (-0.15,0.15)
foreign import ccall safe "DriveMotors"
  c_driveMotors :: CDouble  -- ^ lPow
                -> CDouble  -- ^ rPow
                -> IO ()

-- | Signal the end of phase 1 (go to target)
foreign import ccall safe "SetReturningLed"
  c_setReturningLed :: CUInt  -- ^ val
                    -> IO ()


-- /* Set the state of visiting Led */
foreign import ccall safe "SetVisitingLed"
  c_setVisitingLed :: CUInt
                   -> IO ()

-- | Finish the round
foreign import ccall safe "Finish"
  c_finish :: IO ()

-- /* Broadcast message */
foreign import ccall safe "Say"
  c_say :: CString -- ^ Message
        -> IO ()

------------------------------------------------------------------------------
-- ** Requests

foreign import ccall safe "RequestCompassSensor"
  c_requestCompassSensor :: IO ()

foreign import ccall safe "RequestGroundSensor"
  c_requestGroundSensor :: IO ()

foreign import ccall safe "RequestObstacleSensor"
  c_requestObstacleSensor :: CInt   -- ^ id
                          -> IO ()

foreign import ccall safe "RequestBeaconSensor"
  c_requestBeaconSensor :: CInt   -- ^ id
                        -> IO ()



------------------------------------------------------------------------------
-- * Checking Actions

-- | Verify if setReturningLed was executed
foreign import ccall safe "GetReturningLed"
  c_getReturningLed :: IO CUInt

-- | Verify state of Visiting Led 
foreign import ccall safe "GetVisitingLed"
  c_getVisitingLed :: IO CUInt


-- Verify if Finish() was executed
foreign import ccall safe "GetFinished"
  c_getFinished :: IO CUInt


------------------------------------------------------------------------------
-- * Parameters

-- | Return the simulation cycle time
foreign import ccall safe "GetCycleTime"
  c_getCycleTime :: IO CInt

-- | Return the total simulation time
foreign import ccall safe "GetFinalTime"
  c_getFinalTime :: IO CInt

-- | Return the key time
foreign import ccall safe "GetKeyTime"
  c_getKeyTime :: IO CInt

------------------------------------------------------------------------------
-- ** Functions returning noise levels

-- | Returns maximum additive noise of infra-red sensors
foreign import ccall safe "GetNoiseObstacleSensor"
  c_getNoiseObstacleSensor :: IO CDouble

-- | Returns maximum additive noise of beacon angular direction
foreign import ccall safe "GetNoiseBeaconSensor"
  c_getNoiseBeaconSensor :: IO CDouble

-- | Returns maximum additive noise of compass
foreign import ccall safe "GetNoiseCompassSensor"
  c_getNoiseCompassSensor :: IO CDouble

-- | Returns maximum multipicative noise of motors
foreign import ccall safe "GetNoiseMotors"
  c_getNoiseMotors :: IO CDouble

-- unsigned int  GetNumberRequestsPerCycle(void);
foreign import ccall safe "GetNumberRequestsPerCycle"
  c_getNumberRequestsPerCycle :: IO CInt

-- double GetBeaconAperture();
foreign import ccall safe "GetBeaconAperture"
  c_getBeaconAperture :: IO CInt
 
-- unsigned int GetBeaconLatency();
foreign import ccall safe "GetBeaconLatency"
  c_getBeaconLatency :: IO CInt

-- unsigned int GetIRLatency();
foreign import ccall safe "GetIRLatency"
  c_getIRLatency :: IO CInt

-- unsigned int GetGroundLatency();
foreign import ccall safe "GetGroundLatency"
  c_getGroundLatency :: IO CInt

-- unsigned int GetBumperLatency();
foreign import ccall safe "GetBumperLatency"
  c_getBumperLatency :: IO CInt

-- bool GetBeaconRequestable();
foreign import ccall safe "GetBeaconRequestable"
  c_getBeaconRequestable :: IO CInt

-- bool GetIRRequestable();
foreign import ccall safe "GetIRRequestable"
  c_getIRRequestable :: IO CInt

-- bool GetGroundRequestable();
foreign import ccall safe "GetGroundRequestable"
  c_getGroundRequestable :: IO CInt

  -- bool GetBumperRequestable();
foreign import ccall safe "GetBumperRequestable"
  c_getBumperRequestable :: IO CInt
