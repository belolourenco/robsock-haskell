#include"aux.h"

struct beaconMeasure * GetBeaconSensorWrapper(int x){
   struct beaconMeasure *bm = malloc(sizeof (struct beaconMeasure));
   *bm = GetBeaconSensor(x);
   return bm;
}

void FreeBeaconSensorWrapper(struct beaconMeasure *bm){
   free(bm);
}
