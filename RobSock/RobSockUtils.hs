module RobSock.RobSockUtils where

import RobSock.RobSockMonad
import RobSock.RobSock(BeaconMeasure)

import Control.Monad.State.Lazy
import Control.Applicative


------------------------------------------------------------------------------
-- * Messages

data Message = Msg {
                 mId      :: Int      -- ^ Agent id
               , mContent :: String   -- ^ Content of the message
               }deriving (Show)

-- | Checks pending messages returning a list with the agents' id
availableMsgs :: MM [Int]
availableMsgs = aux 1
  where aux 6 = return []
        aux x = do myPos <- getPos
                   if (x == myPos) 
                     then aux (x+1)
                     else do x' <- newMessageFrom x
                             case x' of
                                True  -> aux (x+1) >>= return.(x:)
                                False -> (aux $ x+1)

-- | Returns the list of available messages
fetchMessages :: MM [Message]
fetchMessages = availableMsgs >>= mapM (\x -> getMessageFrom x 
                                        >>= return.(Msg x))

------------------------------------------------------------------------------
-- * Sensors


data Sensors = Sensors {
                 obsLeft   :: Maybe Double            -- ^ Left obstacle sensor value if available
               , obsRight  :: Maybe Double            -- ^ Right obstacle sensor value if available
               , obsCenter :: Maybe Double            -- ^ Center obstacle sensor value if available
               , obsOther1 :: Maybe Double            -- ^ Other1 obstacle sensor value if available
               , beacon    :: Maybe BeaconMeasure     -- ^ Assuming there is only
                                                      -- one beacon
               , homeBeacon:: Maybe BeaconMeasure     -- ^ Smell sensor for the home beacon if available. EXPERIMENTAL
               , compass   :: Maybe Double            -- ^ Compass value if available
               , bumper    :: Maybe Bool              -- ^ Bumper value if available
               , ground    :: Maybe Int               -- ^ Ground sensor if available
               , gps       :: Maybe (Double, Double)  -- ^ GPS values if available
               } deriving Show

-- | Returns the value of the GPS
getGPS :: MM (Double,Double)
getGPS = do x <- getX
            y <- getY
            return $ (x,y)

-- | Obtains the values of all available sensors
getSensors :: MM Sensors
getSensors = Sensors <$> asksAndGets (isObstacleReady left)   (getObstacleSensor left)
                     <*> asksAndGets (isObstacleReady right)  (getObstacleSensor right)
                     <*> asksAndGets (isObstacleReady center) (getObstacleSensor center)
                     <*> asksAndGets (isObstacleReady other1) (getObstacleSensor other1)
                     <*> asksAndGets (isBeaconReady 0)        (getBeaconSensor 0)
                     <*> asksAndGets (isBeaconReady 1)        (getBeaconSensor 1)
                     <*> asksAndGets isCompassReady           getCompassSensor
                     <*> asksAndGets isBumperReady            getBumperSensor
                     <*> asksAndGets isGroundReady            getGroundSensor
                     <*> asksAndGets isGPSReady               getGPS
  where
    asksAndGets :: (MM Bool) -> (MM a) -> MM (Maybe a)
    asksAndGets a g = do x <- a
                         case x of
                           False -> return Nothing
                           True  -> g >>= return . Just

-- | Returns the value when it is wrapping in a Just or 0 if it is Nothing
mb2Dbl :: Maybe Double -> Double
mb2Dbl Nothing  = 0
mb2Dbl (Just x) = x

-- | Request sensors for the next cycle
nextSensorsReq :: Int -> MM ()
nextSensorsReq x = requestSensors ["IRSensor0", "IRSensor1", "IRSensor2"]

------------------------------------------------------------------------------
-- * Movements

-- | Drive all speed front
fullSpeedFront :: MM ()
fullSpeedFront = driveMotors 0.15 0.15

-- | Drive all speed back
fullSpeedBack :: MM ()
fullSpeedBack = driveMotors (-0.15) (-0.15)

-- | Stops the motors in the next cycle
stopMotors = do l <- getLSpeed
                r <- getRSpeed
                driveMotors (-l) (-r)


data RotateDir = RotLeft | RotRight
               deriving Show

-- | The speed applied when rotating
rotSpeed = 0.05

-- | Rotates in radians in a certain direction
rotate :: Double -> RotateDir -> MM ()
rotate p RotRight = do  l <- getLSpeed
                        r <- getRSpeed
                        driveMotors (-l) (-r)
                        l <- getLSpeed
                        r <- getRSpeed
                        let sl = (l + rotSpeed) / 2
                        let sr = (r - rotSpeed) / 2
                        driveMotors (rotSpeed) (-rotSpeed)
                          >> rotate' sl sr (sl - sr) p
rotate p RotLeft  = do  l <- getLSpeed
                        r <- getRSpeed
                        driveMotors (-l) (-r)
                        l <- getLSpeed
                        r <- getRSpeed
                        let sl = (l - rotSpeed) / 2
                        let sr = (r + rotSpeed) / 2
                        driveMotors (- rotSpeed) (rotSpeed) 
                          >> rotate' sl sr (sr - sl) p

rotate' :: Double -> Double -> Double -> Double -> MM ()
rotate' l r diff p = if p < diff
                     then driveMotors r l 
                     else readSensors 
                          >> driveMotors l r 
                          >> rotate' l r diff (p - diff)
