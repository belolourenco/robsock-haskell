module RobSock.RobSock where

import RobSock.Base.C

import Control.Applicative

import Foreign
import Foreign.C.Types
import Foreign.C.String

------------------------------------------------------------------------------
-- * Utils

-- | Utility function to convert @CUInt@ into a @Bool@
uInt2Bool :: CUInt -> Bool
uInt2Bool x = case fromIntegral x of
                0 -> False
                _ -> True

-- | Utility function to convert @CUChar@ into @Bool@
uChar2Bool :: CUChar -> Bool
uChar2Bool x = case fromIntegral x of
                 0 -> False
                 _ -> True

-- | Utility function to convert @Bool@ into @CUInt@
bool2CUInt :: Bool -> CUInt
bool2CUInt True  = CUInt 1
bool2CUInt False = CUInt 0

------------------------------------------------------------------------------
-- * Constants

-- #define CENTER 0
-- #define LEFT   1
-- #define RIGHT  2
-- #define OTHER1 3

-- | Constant value with the center sensor ID
center :: Int
center = fromIntegral c_center

-- | Constant value with the left sensor ID
left   :: Int
left   = fromIntegral c_left

-- | Constant value with the right sensor ID
right  :: Int
right  = fromIntegral c_right

-- | Constant value with the other1 sensor ID
other1 :: Int
other1 = fromIntegral c_other1

------------------------------------------------------------------------------
-- * Initialization

-- | Initializes Robot and Connects to Simulator 
-- Returns -1 in case of error
initRobot :: String -- ^ Robot name
          -> Int    -- ^ Robot position
          -> String -- ^ Host where the simulator is running
          -> IO Int
initRobot n p h = do n_c     <- newCString n
                     let p_c = CInt $ fromIntegral p
                     h_c     <- newCString h 
                     fromIntegral <$> c_initRobot n_c p_c h_c

--foreign import ccall safe "InitRobot"
--  c_initRobot :: CString -- ^ Robot name
--              -> CInt    -- ^ Robot position
--              -> CString -- ^ Host where the simulator is running
--              -> IO CInt

-- | Initializes Robot and Connects to Simulator 
-- Returns -1 in case of error
initRobot2 :: String     -- ^ Robot name                          
           -> Int        -- ^ Robot position
           -> [Double]   -- ^ IRSensorAngles
           -> String     -- ^ Host where the simulator is running
           -> IO Int
initRobot2 n p i h = do n_c     <- newCString n
                        let p_c = CInt $ fromIntegral p
                        i_c     <- newArray $ map CDouble i
                        h_c     <- newCString h
                        fromIntegral <$> c_initRobot2 n_c p_c i_c h_c
                        
-- | Initializes Robot that also works as the beacon and Connects to Simulator 
--   Returns -1 in case of error
--   extern int InitRobotBeacon(char *name, int id, double height, char *host);
initRobotBeacon :: String  -- ^ Robot name
                -> Int     -- ^ Robot position
                -> Double  -- ^ Height of beacon
                -> String  -- ^ Host where the simulator is running
                -> IO Int
initRobotBeacon n p he h = do n_c     <- newCString n
                              let p_c = CInt $ fromIntegral p
                              let he_c= CDouble he
                              h_c     <- newCString h
                              fromIntegral <$> c_initRobotBeacon n_c p_c he_c h_c
                             
------------------------------------------------------------------------------
-- * Sensors

-- | Gets the next Sensor Values sent by Simulator 
-- if no message is present waits 
-- Returns -1 in case of error 
readSensors :: IO Int
readSensors = fromIntegral <$> c_readSensors

-- ** The following functions access values that have been read by ReadSensors() 
-- they do not read new values 

-- |  simulation time
getTime :: IO Int
getTime = fromIntegral <$> c_getTime

-- | Indicates if a new Obstacle measure from sensor id has arrived. 
-- The value of GetObstacleSensor is invalid when IsObstacleReady returns false
isObstacleReady :: Int     -- ^ id
                -> IO Bool
isObstacleReady id = uInt2Bool <$> c_isObstacleReady (CInt $ fromIntegral id)

-- | GetObstacleSensor value is inversely proportional to obstacle distance  
-- id may be one of LEFT, RIGHT, CENTER or OTHER1 
getObstacleSensor :: Int        -- ^ id
                  -> IO Double
getObstacleSensor id = realToFrac <$> c_getObstacleSensor (CInt $ fromIntegral id)

-- | Indicates if a new beacon measure has arrived. 
-- The value of GetBeaconSensor is invalid when IsBeaconReady returns false
isBeaconReady :: Int      -- ^ id
              -> IO Bool
isBeaconReady id = uInt2Bool <$> c_isBeaconReady (CInt $ fromIntegral id)
 
-- | GetNumberOfBeacons returns the number of beacons in the lab
getNumberOfBeacons :: IO Int
getNumberOfBeacons = fromIntegral <$> c_getNumberOfBeacons

data BeaconMeasure = BM {
    beaconVisible :: Bool
 ,  beacondir     :: Double
} deriving (Show, Eq)

-- | GetBeaconSensor value is the direction of Beacon 
--  in Robot coordinates (-180.0, 180.0) 
getBeaconSensor :: Int -> IO BeaconMeasure
getBeaconSensor id = do bPtr <- c_getBeaconSensor (CInt $ fromIntegral id)
                        b <- peek bPtr
                        c_freeBeaconSensor bPtr
                        return $ BM (uChar2Bool $ c_beaconVisible b)
                                    (realToFrac $ c_beacondir b)

-- | Indicates if a new compass measure has arrived. 
-- The value of GetCompassSensor is invalid when IsCompassReady returns false
isCompassReady :: IO Bool
isCompassReady = uInt2Bool <$> c_isCompassReady

-- | GetCompassSensor value is the direction of Robot in Ground 
-- coordinates (-180.0, 180.0) 
getCompassSensor :: IO Double
getCompassSensor = realToFrac <$> c_getCompassSensor

-- | Indicates if a new ground measure has arrived. 
--  The value of GetGroundSensor is invalid when IsGroundReady returns false
isGroundReady :: IO Bool
isGroundReady = uInt2Bool <$> c_isGroundReady

-- | If robot is inside a target area returns the id of the area, 
-- otherwise returns -1 
getGroundSensor :: IO Int
getGroundSensor = fromIntegral <$> c_getGroundSensor

-- | Indicates if a new bumper measure has arrived. 
-- The value of GetBumperSensor is invalid when IsBumperReady returns false
isBumperReady :: IO Bool
isBumperReady = uInt2Bool <$> c_isBumperReady

-- | Active when robot collides
getBumperSensor :: IO Bool
getBumperSensor = uInt2Bool <$> c_getBumperSensor

-- | Indicates if a new GPS measure has arrived. 
isGPSReady :: IO Bool
isGPSReady = uInt2Bool <$> c_isGPSReady

-- | Indicates if a new GPS Direction measure has arrived. 
isGPSDirReady :: IO Bool
isGPSDirReady = uInt2Bool <$> c_isGPSDirReady

-- | Get coordinate X from GPS sensor
-- GPS sensor - can be used for debug, invoke simulator with "-gps" option
getX :: IO Double
getX = realToFrac <$> c_getX 

-- | Get coordinate Y from GPS sensor
-- The value of getY is invalid when isGPSReady returns false
-- GPS sensor - can be used for debug, invoke simulator with "-gps" option
getY :: IO Double
getY = realToFrac <$> c_getY

-- | Get coordinate Direction from GPS sensor
-- The value of getDir is invalid when isGPSDirReady returns false
-- GPS sensor - can be used for debug, invoke simulator with "-gps" option
getDir :: IO Double
getDir = realToFrac <$> c_getDir

-- | Checks if a new message has arrived
newMessageFrom :: Int     -- ^ id
               -> IO Bool
newMessageFrom id = uInt2Bool <$> c_newMessageFrom (CInt $ fromIntegral id)

-- | Gets message
getMessageFrom :: Int       -- ^ id
               -> IO String
getMessageFrom id = c_getMessageFrom (CInt $ fromIntegral id) >>= peekCString


-- | Request a list of nReqs Sensors
-- Sensors are identified by the following strings: 
-- "Compass", "Ground", "Collision", "IRSensor0", "IRSensor1", etc, "Beacon0", "Beacon1", etc
requestSensors :: [String] -> IO ()
requestSensors l = do l' <- mapM newCString l
                      case length l of
                        1  -> c_requestSensors1 (CInt 1) (head l')
                        2  -> c_requestSensors2 (CInt 2) (head l') (l' !! 1)
                        3  -> c_requestSensors3 (CInt 3) (head l') (l' !! 1) (l' !! 2)
                        4  -> c_requestSensors4 (CInt 4) (head l') (l' !! 1) (l' !! 2)
                                                (l' !! 3)
                        5  -> c_requestSensors5 (CInt 5) (head l') (l' !! 1) (l' !! 2)
                                                (l' !! 3) (l' !! 4)
                        6  -> c_requestSensors6 (CInt 6) (head l') (l' !! 1) (l' !! 2)
                                                (l' !! 3) (l' !! 4) (l' !! 5)
                        7  -> c_requestSensors7 (CInt 7) (head l') (l' !! 1) (l' !! 2)
                                                (l' !! 3) (l' !! 4) (l' !! 5) (l' !! 6)
                        8  -> c_requestSensors8 (CInt 8) (head l') (l' !! 1) (l' !! 2)
                                                (l' !! 3) (l' !! 4) (l' !! 5) (l' !! 6)
                                                (l' !! 7)
                        9  -> c_requestSensors9 (CInt 9) (head l') (l' !! 1) (l' !! 2)
                                                (l' !! 3) (l' !! 4) (l' !! 5) (l' !! 6)
                                                (l' !! 7) (l' !! 8)
                        10 -> c_requestSensors10 (CInt 10) (head l') (l' !! 1) (l' !! 2)
                                                (l' !! 3) (l' !! 4) (l' !! 5) (l' !! 6)
                                                (l' !! 7) (l' !! 8) (l' !! 9)
                        11 -> c_requestSensors11 (CInt 11) (head l') (l' !! 1) (l' !! 2)
                                                (l' !! 3) (l' !! 4) (l' !! 5) (l' !! 6)
                                                (l' !! 7) (l' !! 8) (l' !! 9) (l' !! 10)
                        12 -> c_requestSensors12 (CInt 12) (head l') (l' !! 1) (l' !! 2)
                                                (l' !! 3) (l' !! 4) (l' !! 5) (l' !! 6)
                                                (l' !! 7) (l' !! 8) (l' !! 9) (l' !! 10)
                                                (l' !! 11)
                        13 -> c_requestSensors13 (CInt 13) (head l') (l' !! 1) (l' !! 2)
                                                (l' !! 3) (l' !! 4) (l' !! 5) (l' !! 6)
                                                (l' !! 7) (l' !! 8) (l' !! 9) (l' !! 10)
                                                (l' !! 11) (l' !! 12)

------------------------------------------------------------------------------
-- * Buttons

-- | Start   
getStartButton :: IO Bool
getStartButton = uInt2Bool <$> c_getStartButton


-- | Stop
getStopButton :: IO Bool
getStopButton = uInt2Bool <$> c_getStopButton

------------------------------------------------------------------------------
-- * Actions

-- | Drive right motor with rPow and left motor with lPow
-- Powers in (-0.15,0.15)
driveMotors :: Double -- ^ lPow
            -> Double -- ^ rPow
            -> IO ()
driveMotors l r = c_driveMotors (CDouble l) (CDouble r)

-- | Signal the end of phase 1 (go to target)
setReturningLed :: Bool  -- ^ val
                -> IO ()
setReturningLed b = c_setReturningLed (bool2CUInt b)

-- /* Set the state of visiting Led */
setVisitingLed :: Bool -> IO ()
setVisitingLed b = c_setVisitingLed (bool2CUInt b)

-- | Finish the round
finish :: IO ()
finish = c_finish

-- /* Broadcast message */
say :: String -- ^ Message
    -> IO ()
say s = newCString s >>= c_say

------------------------------------------------------------------------------
-- ** Requests

requestCompassSensor :: IO ()
requestCompassSensor = c_requestCompassSensor

requestGroundSensor :: IO ()
requestGroundSensor = c_requestGroundSensor  

requestObstacleSensor :: Int    -- ^ id
                      -> IO ()
requestObstacleSensor id = c_requestObstacleSensor (CInt $ fromIntegral id)

requestBeaconSensor :: Int    -- ^ id
                    -> IO ()
requestBeaconSensor id = c_requestBeaconSensor (CInt $ fromIntegral id)

------------------------------------------------------------------------------
-- * Checking Actions

-- | Verify if setReturningLed was executed
getReturningLed :: IO Bool
getReturningLed = uInt2Bool <$> c_getReturningLed

-- | Verify state of Visiting Led 
getVisitingLed :: IO Bool
getVisitingLed = uInt2Bool <$> c_getVisitingLed

-- | Verify if Finish() was executed
getFinished :: IO Bool
getFinished = uInt2Bool <$> c_getFinished

------------------------------------------------------------------------------
-- * Parameters

-- | Return the simulation cycle time
getCycleTime :: IO Int
getCycleTime = fromIntegral <$> c_getCycleTime

-- | Return the total simulation time
getFinalTime :: IO Int
getFinalTime = fromIntegral <$> c_getFinalTime 

-- | Return the key time
getKeyTime :: IO Int
getKeyTime = fromIntegral <$> c_getKeyTime

------------------------------------------------------------------------------
-- ** Functions returning noise levels

-- | Returns maximum additive noise of infra-red sensors
getNoiseObstacleSensor :: IO Double
getNoiseObstacleSensor = realToFrac <$> c_getNoiseObstacleSensor

-- | Returns maximum additive noise of beacon angular direction
getNoiseBeaconSensor :: IO Double
getNoiseBeaconSensor = realToFrac <$> c_getNoiseBeaconSensor

-- | Returns maximum additive noise of compass
getNoiseCompassSensor :: IO Double
getNoiseCompassSensor = realToFrac <$> c_getNoiseCompassSensor

-- | Returns maximum multipicative noise of motors
getNoiseMotors :: IO Double
getNoiseMotors = realToFrac <$> c_getNoiseMotors

-- unsigned int  GetNumberRequestsPerCycle(void);
getNumberRequestsPerCycle :: IO Int
getNumberRequestsPerCycle = fromIntegral <$> c_getNumberRequestsPerCycle

-- double GetBeaconAperture();
getBeaconAperture :: IO Int
getBeaconAperture = fromIntegral <$> c_getBeaconAperture
 
-- unsigned int GetBeaconLatency();
getBeaconLatency :: IO Int
getBeaconLatency = fromIntegral <$> getBeaconLatency

-- unsigned int GetIRLatency();
getIRLatency :: IO Int
getIRLatency = fromIntegral <$> c_getIRLatency

-- unsigned int GetGroundLatency();
getGroundLatency :: IO Int
getGroundLatency = fromIntegral <$> getGroundLatency

-- unsigned int GetBumperLatency();
getBumperLatency :: IO Int
getBumperLatency = fromIntegral <$> c_getBumperLatency

-- bool GetBeaconRequestable();
getBeaconRequestable :: IO Int
getBeaconRequestable = fromIntegral <$> c_getBeaconRequestable

-- bool GetIRRequestable();
getIRRequestable :: IO Int
getIRRequestable = fromIntegral <$> c_getIRRequestable

-- bool GetGroundRequestable();
getGroundRequestable :: IO Int
getGroundRequestable = fromIntegral <$> c_getGroundRequestable

-- bool GetBumperRequestable();
getBumperRequestable :: IO Int
getBumperRequestable = fromIntegral <$> c_getBumperRequestable
