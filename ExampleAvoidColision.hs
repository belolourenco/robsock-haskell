module ExampleAvoidColision where

import RobSock.RobSockMonad
import RobSock.RobSockUtils
import RobSock.RobSock(BeaconMeasure(BM))

import Control.Monad
import Control.Monad.Trans
import Control.Monad.State
import System.Environment

debug = True

debugShow :: String -> MM ()
debugShow x = case debug of
                True  -> lift.putStrLn $ x
                False -> return ()

main :: IO ()
main = do [name,pos,host] <- getArgs
          (x,_) <- runStateT (start name (read pos) host) NoMouse
          case x of
            (-1) -> putStrLn "Failed to connect"
            _    -> putStrLn "Connection finished"

start :: String -> Int -> String -> MM Int
start n p h = do get >>= debugShow.show
                 x <- initRobot n p h 
                 case x of
                   (-1) -> return $ -1
                   y    -> waitStart >> return 0

waitStart:: MM ()
waitStart = do get >>= debugShow.show
               readSensors
               getLSpeed >>= \l -> getRSpeed 
                         >>= debugShow.(("Speed:" ++ show l) ++).show
               x <- getStartButton 
               case x of
                 True  -> moveNoColision 0
                 False -> waitStart

moveNoColision :: Int -> MM ()
moveNoColision c = do get >>= debugShow.show  
                      readSensors
                      sensors <- getSensors
                      debugShow.show $ sensors
                      decide sensors
                      reqObstacleOnly
                      moveNoColision (c+1)

decide :: Sensors -> MM ()
decide s = if bumperTrue s
           then if mb2d (obsLeft s) > mb2d (obsRight s)
                then rotate (pi/4) RotRight 
                else rotate (pi/4) RotLeft
           else if mb2d (obsCenter s) > 2.5
                then if mb2d (obsLeft s) > mb2d (obsRight s)
                     then rotate (pi/4) RotRight
                     else rotate (pi/4) RotLeft
                else if mb2d (obsLeft s) > 2.5
                     then rotate (pi/4) RotRight
                     else if mb2d (obsRight s) > 2.5 
                          then rotate (pi/4) RotLeft
                          else driveMotors 0.1 0.1
  where mb2d :: Maybe Double -> Double
        mb2d Nothing = 0
        mb2d (Just x)= x

bumperTrue :: Sensors -> Bool
bumperTrue s = case bumper s of
                 Nothing -> False
                 (Just b)-> b

reqObstacleOnly :: MM ()
reqObstacleOnly = requestSensors ["IRSensor0", "IRSensor1", "IRSensor2"]
